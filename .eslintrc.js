module.exports = {
  "extends": "airbnb",
  "settings": {
      "import/resolver": {
          "node": {
              "extensions": [".js", ".ios.js", ".android.js"]
          }
      },
  },
  "parser": "babel-eslint",
  "plugins": [
      "jest",
      "react-native"
  ],
  "env": {
      "node": true,
      "jest/globals": true,
      "react-native/react-native": true
  },
  "rules": {
      "react/jsx-filename-extension": 0,
      "react-native/no-unused-styles": 0,
      "react-native/split-platform-components": 2,
      "react-native/no-inline-styles": 0,
      "react-native/no-color-literals": 0,
      "react/state-in-constructor": 0,
      "react/jsx-props-no-spreading": 0
  },
  "globals": {
      "fetch": false,
  }
};
