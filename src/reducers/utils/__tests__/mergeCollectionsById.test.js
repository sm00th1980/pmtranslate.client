import { mergeCollectionsById } from '..';

describe('mergeCollectionsById', () => {
  it('mergeCollectionsById', () => {
    const oldCollection = [{ id: 1 }];
    const newCollection = [{ id: 2 }];

    expect(mergeCollectionsById(oldCollection, newCollection)).toEqual([{
      id: 1,
    }, {
      id: 2,
    }]);
  });

  it('mergeCollectionsById', () => {
    const oldCollection = [{ id: 1 }, { id: 2 }];
    const newCollection = [{ id: 3 }];

    expect(mergeCollectionsById(oldCollection, newCollection)).toEqual([{
      id: 1,
    }, {
      id: 2,
    }, {
      id: 3,
    }]);
  });

  it('mergeCollectionsById', () => {
    const oldCollection = [{ id: 1 }];
    const newCollection = [{ id: 1 }];

    expect(mergeCollectionsById(oldCollection, newCollection)).toEqual([{
      id: 1,
    }]);
  });

  it('mergeCollectionsById', () => {
    const oldCollection = undefined;
    const newCollection = [{ id: 1 }];

    expect(mergeCollectionsById(oldCollection, newCollection)).toEqual([{
      id: 1,
    }]);
  });

  it('mergeCollectionsById', () => {
    const oldCollection = [{ id: 1 }];
    const newCollection = undefined;

    expect(mergeCollectionsById(oldCollection, newCollection)).toEqual([{
      id: 1,
    }]);
  });

  it('mergeCollectionsById', () => {
    const oldCollection = undefined;
    const newCollection = [undefined];

    expect(mergeCollectionsById(oldCollection, newCollection)).toBeUndefined();
  });

  it('mergeCollectionsById', () => {
    const oldCollection = undefined;
    const newCollection = undefined;

    expect(mergeCollectionsById(oldCollection, newCollection)).toBeUndefined();
  });

  it('mergeCollectionsById', () => {
    const oldCollection = [undefined];
    const newCollection = undefined;

    expect(mergeCollectionsById(oldCollection, newCollection)).toBeUndefined();
  });

  it('mergeCollectionsById', () => {
    const oldCollection = [undefined];
    const newCollection = [undefined];

    expect(mergeCollectionsById(oldCollection, newCollection)).toBeUndefined();
  });
});
