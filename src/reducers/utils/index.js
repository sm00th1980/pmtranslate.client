import { reject, isNil, isEmpty } from 'ramda';

export const replaceItemById = (items, newItem) => items.map((item) => {
  if (item.id === newItem.id) {
    return newItem;
  }

  return item;
});

const compact = (items) => {
  if (isNil(items)) {
    return [];
  }

  return reject((item) => isNil(item), items);
};

const isNilOrEmpty = (items) => {
  if (isNil(items)) {
    return true;
  }

  return isEmpty(compact(items));
};

export const mergeCollectionsById = (oldCollection, newCollection) => {
  if (isNilOrEmpty(oldCollection) && isNilOrEmpty(newCollection)) {
    return undefined;
  }

  if (!isNilOrEmpty(oldCollection) && isNilOrEmpty(newCollection)) {
    return oldCollection;
  }

  if (isNilOrEmpty(oldCollection) && !isNilOrEmpty(newCollection)) {
    return newCollection;
  }

  const newCollectionIds = newCollection.map((item) => item.id);
  const notPresentedItemsFromOldCollection = reject(
    (item) => newCollectionIds.includes(item.id),
    oldCollection,
  );

  return [...notPresentedItemsFromOldCollection, ...newCollection];
};
