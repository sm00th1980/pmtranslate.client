import { flatten } from 'ramda';
import { replaceItemById, mergeCollectionsById } from './utils';

import {
  FETCH_SECTIONS_SUCCESS,
  FETCH_FILES_SUCCESS,
  TOGGLE_TRANSLATION_APPROVE,
} from '../constants/actions';

const initialState = {
  sections: undefined,
  files: [],
  translations: [],
};

export default function (state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_SECTIONS_SUCCESS:
      return { ...state, sections: [...payload] };

    case FETCH_FILES_SUCCESS:
      return {
        ...state,
        files: mergeCollectionsById(state.files, payload),
        translations: mergeCollectionsById(
          state.translations,
          flatten(payload.map((file) => file.translations)),
        ),
      };

    case TOGGLE_TRANSLATION_APPROVE:
      return {
        ...state,
        translations: replaceItemById(state.translations, payload),
      };

    default:
      return state;
  }
}
