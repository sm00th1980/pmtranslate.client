import withQuery from 'with-query';
import { API_URL } from '../../../constants/api';

export const get = (url, params = {}, Fetch = fetch) => Fetch(withQuery(`${API_URL}${url}`, params), { credentials: 'same-origin' }).then((response) => response.json());

export const post = (url, params = {}, Fetch = fetch) => {
  const data = {
    method: 'POST',
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
    body: JSON.stringify(params),
    credentials: 'same-origin',
  };

  return Fetch(`${API_URL}${url}`, data).then((response) => response.json());
};
