import { get } from '..';
import { API_URL } from '../../../../constants/api';

describe('get()', () => {
  it('make get request with success without params', async () => {
    expect.assertions(3);
    const response = 'response';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockParams = {};
    const mockFetch = jest.fn(() => Promise.resolve({ json: () => response }));

    const url = '/url';

    await get(url, mockParams, mockFetch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith(response);
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockFetch).toHaveBeenCalledWith(`${API_URL}/url`, {
      credentials: 'same-origin',
    });
  });

  it('make get request with failure', async () => {
    expect.assertions(3);
    const error = new Error();

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockParams = {};
    const mockFetch = jest.fn(() => Promise.reject(error));

    const url = '/url';

    await get(url, mockParams, mockFetch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error);

    expect(mockFetch).toHaveBeenCalledWith(`${API_URL}/url`, {
      credentials: 'same-origin',
    });
  });

  it('make get request with success with params', async () => {
    expect.assertions(3);
    const response = 'response';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockParams = {
      param1: 'param1',
    };
    const mockFetch = jest.fn(() => Promise.resolve({ json: () => response }));

    const url = '/url';

    await get(url, mockParams, mockFetch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith(response);
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockFetch).toHaveBeenCalledWith(`${API_URL}/url?param1=param1`, {
      credentials: 'same-origin',
    });
  });
});
