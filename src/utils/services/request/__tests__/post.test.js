import { post } from '..';
import { API_URL } from '../../../../constants/api';

describe('post()', () => {
  it('make post request with success', async () => {
    expect.assertions(3);
    const response = 'response';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockFetch = jest.fn(() => Promise.resolve({ json: () => response }));

    const url = '/url';
    const params = { param1: 1, param2: 2 };

    await post(url, params, mockFetch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith(response);
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockFetch).toHaveBeenCalledWith(`${API_URL}/url`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
      credentials: 'same-origin',
    });
  });

  it('make post request with failure', async () => {
    expect.assertions(3);
    const error = new Error();

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockFetch = jest.fn(() => Promise.reject(error));

    const url = '/url';
    const params = { param1: 1, param2: 2 };

    await post(url, params, mockFetch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error);

    expect(mockFetch).toHaveBeenCalledWith(`${API_URL}/url`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
      credentials: 'same-origin',
    });
  });
});
