export const ACCESS_TOKEN = 'ACCESS_TOKEN';
export const USERNAME = 'USERNAME';

export const saveAuthCredentialsDef = (asyncStorage, username, accessToken) => {
  const saveUsername = asyncStorage.setItem(USERNAME, username);
  const saveAccessToken = asyncStorage.setItem(ACCESS_TOKEN, accessToken);

  return Promise.all([saveUsername, saveAccessToken]);
};

export const getAuthCredentialsDef = async (asyncStorage) => {
  const username = await asyncStorage.getItem(USERNAME);
  const accessToken = await asyncStorage.getItem(ACCESS_TOKEN);

  if (username && accessToken) {
    return { username, accessToken };
  }

  return null;
};
