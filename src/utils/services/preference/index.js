import { AsyncStorage } from 'react-native';
import { partial } from 'ramda';
import { saveAuthCredentialsDef, getAuthCredentialsDef } from './preference';

export const saveAuthCredentials = partial(saveAuthCredentialsDef, [AsyncStorage]);
export const getAuthCredentials = partial(getAuthCredentialsDef, [AsyncStorage]);
