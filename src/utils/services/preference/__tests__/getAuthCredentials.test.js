import {
  getAuthCredentialsDef as getAuthCredentials,
  USERNAME,
  ACCESS_TOKEN,
} from '../preference';

describe('getAuthCredentials()', () => {
  it('get auth credentials with success', async () => {
    expect.assertions(3);

    const username = 'username';
    const accessToken = 'accessToken';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGetItem = jest
      .fn()
      .mockResolvedValueOnce(username)
      .mockResolvedValueOnce(accessToken);
    const mockAsyncStorage = { getItem: mockGetItem };

    await getAuthCredentials(mockAsyncStorage)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith({
      username,
      accessToken,
    });
    expect(onFailure).not.toHaveBeenCalled();
    expect(mockGetItem.mock.calls).toEqual([[USERNAME], [ACCESS_TOKEN]]);
  });

  it('get auth credentials with fail', async () => {
    expect.assertions(3);

    const username = null;
    const accessToken = null;

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGetItem = jest
      .fn()
      .mockResolvedValueOnce(username)
      .mockResolvedValueOnce(accessToken);
    const mockAsyncStorage = { getItem: mockGetItem };

    await getAuthCredentials(mockAsyncStorage)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith(null);
    expect(onFailure).not.toHaveBeenCalled();
    expect(mockGetItem.mock.calls).toEqual([[USERNAME], [ACCESS_TOKEN]]);
  });
});
