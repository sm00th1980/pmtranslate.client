import {
  saveAuthCredentialsDef as saveAuthCredentials,
  USERNAME,
  ACCESS_TOKEN,
} from '../preference';

describe('saveAuthCredentials()', () => {
  it('save auth credentials with success', async () => {
    expect.assertions(3);

    const username = 'username';
    const accessToken = 'accessToken';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockSetItem = jest
      .fn()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce();
    const mockAsyncStorage = { setItem: mockSetItem };

    await saveAuthCredentials(mockAsyncStorage, username, accessToken)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalled();
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockSetItem.mock.calls).toEqual([
      [USERNAME, username],
      [ACCESS_TOKEN, accessToken],
    ]);
  });
});
