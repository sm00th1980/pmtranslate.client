import { splitByParts } from '..';

describe('splitByParts', () => {
  it('split array on several parts', () => {
    const input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    expect(splitByParts(3, input)).toEqual([
      [1, 4, 7, 10],
      [2, 5, 8],
      [3, 6, 9],
    ]);
  });

  it('split array on several parts', () => {
    const input = [1];

    expect(splitByParts(3, input)).toEqual([
      [1],
      [],
      [],
    ]);
  });

  it('split array on several parts', () => {
    const input = [1, 2];

    expect(splitByParts(3, input)).toEqual([
      [1],
      [2],
      [],
    ]);
  });

  it('split array on several parts', () => {
    const input = [1, 2, 3];

    expect(splitByParts(3, input)).toEqual([
      [1],
      [2],
      [3],
    ]);
  });

  it('split array on several parts', () => {
    const input = [1, 2, 3, 4];

    expect(splitByParts(3, input)).toEqual([
      [1, 4],
      [2],
      [3],
    ]);
  });

  it('split array on several parts', () => {
    const input = ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9'];

    expect(splitByParts(2, input)).toEqual([
      ['a1', 'a3', 'a5', 'a7', 'a9'],
      ['a2', 'a4', 'a6', 'a8'],
    ]);
  });

  it('split array on several parts', () => {
    const input = ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9'];

    expect(splitByParts(4, input)).toEqual([
      ['a1', 'a5', 'a9'],
      ['a2', 'a6'],
      ['a3', 'a7'],
      ['a4', 'a8'],
    ]);
  });
});
