import { sampleByIndex } from '..';

describe('sampleByIndex', () => {
  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    const index = 0;
    const length = 3;

    expect(sampleByIndex(index, length, items)).toBe(2);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    const index = 1;
    const length = 3;

    expect(sampleByIndex(index, length, items)).toBe(4);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    const index = 2;
    const length = 3;

    expect(sampleByIndex(index, length, items)).toBe(6);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    const index = 0;
    const length = 4;

    expect(sampleByIndex(index, length, items)).toBe(1);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    const index = 1;
    const length = 4;

    expect(sampleByIndex(index, length, items)).toBe(3);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    const index = 2;
    const length = 4;

    expect(sampleByIndex(index, length, items)).toBe(5);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    const index = 3;
    const length = 4;

    expect(sampleByIndex(index, length, items)).toBe(7);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6];
    const index = 0;
    const length = 4;

    expect(sampleByIndex(index, length, items)).toBe(1);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6];
    const index = 1;
    const length = 4;

    expect(sampleByIndex(index, length, items)).toBe(2);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6];
    const index = 2;
    const length = 4;

    expect(sampleByIndex(index, length, items)).toBe(4);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3, 4, 5, 6];
    const index = 3;
    const length = 4;

    expect(sampleByIndex(index, length, items)).toBe(5);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2];
    const index = 0;
    const length = 6;

    expect(sampleByIndex(index, length, items)).toBe(0);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2];
    const index = 1;
    const length = 6;

    expect(sampleByIndex(index, length, items)).toBe(0);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2];
    const index = 2;
    const length = 6;

    expect(sampleByIndex(index, length, items)).toBe(1);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2];
    const index = 3;
    const length = 6;

    expect(sampleByIndex(index, length, items)).toBe(1);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2];
    const index = 4;
    const length = 6;

    expect(sampleByIndex(index, length, items)).toBe(2);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2];
    const index = 5;
    const length = 6;

    expect(sampleByIndex(index, length, items)).toBe(2);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3];
    const index = 0;
    const length = 7;

    expect(sampleByIndex(index, length, items)).toBe(0);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3];
    const index = 1;
    const length = 7;

    expect(sampleByIndex(index, length, items)).toBe(0);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3];
    const index = 2;
    const length = 7;

    expect(sampleByIndex(index, length, items)).toBe(1);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3];
    const index = 3;
    const length = 7;

    expect(sampleByIndex(index, length, items)).toBe(1);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3];
    const index = 4;
    const length = 7;

    expect(sampleByIndex(index, length, items)).toBe(2);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3];
    const index = 5;
    const length = 7;

    expect(sampleByIndex(index, length, items)).toBe(2);
  });

  it('return sample by nearest index', () => {
    const items = [0, 1, 2, 3];
    const index = 6;
    const length = 7;

    expect(sampleByIndex(index, length, items)).toBe(3);
  });
});
