import { addKeys } from '..';

describe('addKeys', () => {
  it('add keys as id if id present', () => {
    const items = [{
      id: 0,
    }, {
      id: 1,
    }];

    expect(addKeys(items)).toEqual([{
      id: 0,
      key: '0',
    }, {
      id: 1,
      key: '1',
    }]);
  });

  it('add keys as index if id not present', () => {
    const items = [{
    }, {
    }];

    expect(addKeys(items)).toEqual([{
      key: '0',
    }, {
      key: '1',
    }]);
  });
});
