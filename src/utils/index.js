import {
  values, groupBy, times, pathOr, find, propEq,
} from 'ramda';

export const noop = () => {};

export const splitByParts = (count, list) => {
  const groupFn = ({ index }) => index % count;
  const result = values(groupBy(groupFn, list.map((element, index) => ({ element, index }))))
    .map((elements) => elements.map(({ element }) => element));

  return [...result, ...times(() => [], count - result.length)];
};

const key = (item, index) => (pathOr(index, ['id'], item).toString());

export const addKeys = (items) => items
  .map((item, index) => ({ ...item, key: key(item, index) }));

export const findById = (id, items) => find(propEq('id', id), items);

export const sample = (items) => items[Math.floor(Math.random() * items.length)];


export const sampleByIndex = (index, length, items) => {
  if (length === items.length) {
    return items[index];
  }

  if (items.length > length) {
    const percents = (1 / (length + 1)) * index + 1 / (length + 1);
    const newIndex = Math.floor(items.length * percents);
    return items[newIndex];
  }

  const percents = ((index * 100) / length) / 100;
  const newIndex = Math.floor(items.length * percents);

  return items[newIndex];
};
