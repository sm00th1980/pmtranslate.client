import {
  FETCH_SECTIONS_SUCCESS,
  FETCH_FILES_SUCCESS,
  TOGGLE_TRANSLATION_APPROVE,
} from '../constants/actions';

export const fetchSectionsSuccess = (payload) => ({
  type: FETCH_SECTIONS_SUCCESS,
  payload,
});

export const fetchFilesSuccess = (payload) => ({
  type: FETCH_FILES_SUCCESS,
  payload,
});

export const toggleTranslationApprove = (payload) => ({
  type: TOGGLE_TRANSLATION_APPROVE,
  payload,
});
