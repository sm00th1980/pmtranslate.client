import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import Auth from '../components/screens/Auth';
import Sections from '../components/screens/Sections';
import Files from '../components/screens/Files';

import NavigationService from '../utils/services/navigation';

const MainNavigator = createStackNavigator(
  {
    Sections,
    Files,
  },
  {
    initialRouteName: 'Sections',
    headerMode: 'none',
  },
);

const AppNavigator = createStackNavigator({
  Auth,
  Main: MainNavigator,
}, {
  mode: 'modal',
  headerMode: 'none',
  initialRouteName: 'Main',
});

const AppContainer = createAppContainer(AppNavigator);

export default class extends React.Component {
  render() {
    return (
      <AppContainer
        ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
