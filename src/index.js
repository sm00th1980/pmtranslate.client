import React from 'react';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import Navigator from './navigator';

import Setup from './setup';

Setup();

const store = configureStore({});

const App = () => (
  <Provider store={store}>
    <Navigator />
  </Provider>
);

export default App;
