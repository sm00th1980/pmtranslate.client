// setup file
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Setup from './setup';

Setup();

configure({ adapter: new Adapter() });
