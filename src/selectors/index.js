import { pathOr } from 'ramda';

import { noop } from '../utils';

export const getGoBack = (props) => pathOr(noop, ['navigation', 'goBack'], props);
export const getGetParam = (props) => pathOr(noop, ['navigation', 'getParam'], props);
export const getNavigate = (props) => pathOr(noop, ['navigation', 'navigate'], props);

export const getFiles = (state) => pathOr([], ['data', 'files'], state);
export const getTranslations = (state) => pathOr([], ['data', 'translations'], state);
export const getSections = (state) => pathOr([], ['data', 'sections'], state);
