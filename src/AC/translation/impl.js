/* eslint import/prefer-default-export: off */

import {
  isNil, pathOr,
} from 'ramda';

import {
  toggleTranslationApprove,
} from '../../actions';
import { findById } from '../../utils';

export const toggleTranslationApprovedDef = (
  Post,
  getAuthCredentials,
  navigate,
  translationId,
) => async (dispatch, getState) => {
  const auth = await getAuthCredentials();

  if (isNil(auth)) {
    navigate('Auth');
    return Promise.resolve();
  }
  const translations = pathOr([], ['data', 'translations'], getState());
  const translation = findById(translationId, translations);

  const { id, approved } = translation;

  const params = { username: auth.username, access_token: auth.accessToken };
  return Post(`/translations/${id}/toggle_approved`, params)
    .then((response) => {
      if (response.success) {
        dispatch(
          toggleTranslationApprove({
            ...translation,
            approved: !approved,
          }),
        );
      } else {
        throw Error(response.error);
      }
    });
};
