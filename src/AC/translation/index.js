/* eslint import/prefer-default-export: off */

import { partial } from 'ramda';
import { post } from '../../utils/services/request';
import { getAuthCredentials } from '../../utils/services/preference';
import NavigationService from '../../utils/services/navigation';

import { toggleTranslationApprovedDef } from './impl';

export const toggleTranslationApproved = partial(
  toggleTranslationApprovedDef,
  [
    post,
    getAuthCredentials,
    NavigationService.navigate,
  ],
);
