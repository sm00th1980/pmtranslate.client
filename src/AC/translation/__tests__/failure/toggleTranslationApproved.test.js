import { toggleTranslationApprovedDef as toggleTranslationApproved } from '../../impl';

describe('toggleTranslationApproved()', () => {
  it('toggle translation approve when request succeed from false to true', async () => {
    expect.assertions(6);

    const authCredentials = { username: 'username', accessToken: 'accessToken' };

    const response = { success: false };

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const navigate = jest.fn();
    const fileId = 2;
    const translation = { id: 1, approved: false, file_id: fileId };
    const mockPost = jest.fn(() => Promise.resolve(response));
    const mockGetAuthCredentials = jest.fn().mockResolvedValueOnce(authCredentials);

    const mockDispatch = jest.fn();
    const mockGetState = () => ({
      data: {
        translations: [
          translation,
        ],
      },
    });

    await toggleTranslationApproved(
      mockPost,
      mockGetAuthCredentials,
      navigate,
      translation.id,
    )(
      mockDispatch,
      mockGetState,
    )
      .then(onSuccess)
      .catch(onFailure);

    expect(navigate).not.toHaveBeenCalled();
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalled();

    expect(mockDispatch).not.toHaveBeenCalled();

    expect(mockPost).toHaveBeenCalledWith(`/translations/${translation.id}/toggle_approved`, {
      username: authCredentials.username,
      access_token: authCredentials.accessToken,
    });
    expect(mockGetAuthCredentials).toHaveBeenCalled();
  });

  it('toggle translation approve when request succeed from false to true', async () => {
    expect.assertions(6);

    const authCredentials = { username: 'username', accessToken: 'accessToken' };

    const error = new Error();

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const navigate = jest.fn();
    const fileId = 2;
    const translation = { id: 1, approved: false, file_id: fileId };
    const mockPost = jest.fn(() => Promise.reject(error));
    const mockGetAuthCredentials = jest.fn().mockResolvedValueOnce(authCredentials);

    const mockDispatch = jest.fn();
    const mockGetState = () => ({
      data: {
        translations: [
          translation,
        ],
      },
    });

    await toggleTranslationApproved(
      mockPost,
      mockGetAuthCredentials,
      navigate,
      translation.id,
    )(
      mockDispatch,
      mockGetState,
    )
      .then(onSuccess)
      .catch(onFailure);

    expect(navigate).not.toHaveBeenCalled();
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error);

    expect(mockDispatch).not.toHaveBeenCalled();

    expect(mockPost).toHaveBeenCalledWith(`/translations/${translation.id}/toggle_approved`, {
      username: authCredentials.username,
      access_token: authCredentials.accessToken,
    });
    expect(mockGetAuthCredentials).toHaveBeenCalled();
  });
});
