import { toggleTranslationApprovedDef as toggleTranslationApproved } from '../../impl';
import {
  TOGGLE_TRANSLATION_APPROVE,
} from '../../../../constants/actions';

describe('toggleTranslationApproved()', () => {
  it('not toggle translation approve when no auth', async () => {
    expect.assertions(6);

    const authCredentials = undefined;

    const response = { success: true };

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const navigate = jest.fn();
    const fileId = 2;
    const translation = { id: 1, approved: false, file_id: fileId };
    const mockPost = jest.fn(() => Promise.resolve(response));
    const mockGetAuthCredentials = jest.fn().mockResolvedValueOnce(authCredentials);

    const mockDispatch = jest.fn();
    const mockGetState = () => ({
      data: {
        translations: [
          translation,
        ],
      },
    });

    await toggleTranslationApproved(
      mockPost,
      mockGetAuthCredentials,
      navigate,
      translation.id,
    )(
      mockDispatch,
      mockGetState,
    )
      .then(onSuccess)
      .catch(onFailure);

    expect(navigate).toHaveBeenCalledWith('Auth');
    expect(onSuccess).toHaveBeenCalled();
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockDispatch).not.toHaveBeenCalled();
    expect(mockPost).not.toHaveBeenCalled();
    expect(mockGetAuthCredentials).toHaveBeenCalled();
  });

  it('toggle translation approve when request succeed from false to true', async () => {
    expect.assertions(6);

    const authCredentials = { username: 'username', accessToken: 'accessToken' };

    const response = { success: true };

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const navigate = jest.fn();
    const fileId = 2;
    const translation = { id: 1, approved: false, file_id: fileId };
    const mockPost = jest.fn(() => Promise.resolve(response));
    const mockGetAuthCredentials = jest.fn().mockResolvedValueOnce(authCredentials);

    const mockDispatch = jest.fn();
    const mockGetState = () => ({
      data: {
        translations: [
          translation,
        ],
      },
    });

    await toggleTranslationApproved(
      mockPost,
      mockGetAuthCredentials,
      navigate,
      translation.id,
    )(
      mockDispatch,
      mockGetState,
    )
      .then(onSuccess)
      .catch(onFailure);

    expect(navigate).not.toHaveBeenCalled();
    expect(onSuccess).toHaveBeenCalled();
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockDispatch.mock.calls).toEqual([
      [
        {
          type: TOGGLE_TRANSLATION_APPROVE,
          payload: { ...translation, approved: true },
        },
      ],
    ]);

    expect(mockPost).toHaveBeenCalledWith(`/translations/${translation.id}/toggle_approved`, {
      username: authCredentials.username,
      access_token: authCredentials.accessToken,
    });
    expect(mockGetAuthCredentials).toHaveBeenCalled();
  });

  it('toggle translation approve when request succeed from true to false', async () => {
    expect.assertions(6);

    const authCredentials = { username: 'username', accessToken: 'accessToken' };

    const response = { success: true };

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const navigate = jest.fn();
    const fileId = 2;
    const translation = { id: 1, approved: true, file_id: fileId };
    const mockPost = jest.fn(() => Promise.resolve(response));
    const mockGetAuthCredentials = jest.fn().mockResolvedValueOnce(authCredentials);

    const mockDispatch = jest.fn();
    const mockGetState = () => ({
      data: {
        translations: [
          translation,
        ],
      },
    });

    await toggleTranslationApproved(
      mockPost,
      mockGetAuthCredentials,
      navigate,
      translation.id,
    )(
      mockDispatch,
      mockGetState,
    )
      .then(onSuccess)
      .catch(onFailure);

    expect(navigate).not.toHaveBeenCalled();
    expect(onSuccess).toHaveBeenCalled();
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockDispatch.mock.calls).toEqual([
      [
        {
          type: TOGGLE_TRANSLATION_APPROVE,
          payload: { ...translation, approved: false },
        },
      ],
    ]);

    expect(mockPost).toHaveBeenCalledWith(`/translations/${translation.id}/toggle_approved`, {
      username: authCredentials.username,
      access_token: authCredentials.accessToken,
    });
    expect(mockGetAuthCredentials).toHaveBeenCalled();
  });
});
