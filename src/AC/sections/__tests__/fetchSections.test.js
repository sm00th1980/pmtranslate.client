import { fetchSectionsDef as fetchSections } from '../impl';
import {
  FETCH_SECTIONS_SUCCESS,
} from '../../../constants/actions';

describe('fetchSections()', () => {
  it('fetch sections when request succeed', async () => {
    expect.assertions(4);

    const response = {
      success: true,
      sections: 'sections',
    };

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn().mockResolvedValueOnce(response);
    const mockDispatch = jest.fn();

    await fetchSections(mockGet)(mockDispatch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalled();
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockDispatch.mock.calls).toEqual([
      [{ type: FETCH_SECTIONS_SUCCESS, payload: response.sections }],
    ]);

    expect(mockGet).toHaveBeenCalledWith('/sections');
  });

  it('not fetch sections when request succeed but response not success', async () => {
    expect.assertions(4);

    const response = {
      success: false,
    };

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn().mockResolvedValueOnce(response);
    const mockDispatch = jest.fn();

    await fetchSections(mockGet)(mockDispatch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalled();

    expect(mockDispatch).not.toHaveBeenCalled();

    expect(mockGet).toHaveBeenCalledWith('/sections');
  });

  it('not fetch sections when request succeed but response not success', async () => {
    expect.assertions(4);

    const error = new Error('');

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn().mockRejectedValueOnce(error);
    const mockDispatch = jest.fn();

    await fetchSections(mockGet)(mockDispatch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error);

    expect(mockDispatch).not.toHaveBeenCalled();

    expect(mockGet).toHaveBeenCalledWith('/sections');
  });
});
