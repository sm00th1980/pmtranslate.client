/* eslint import/prefer-default-export: off */

import { partial } from 'ramda';
import { get } from '../../utils/services/request';

import { fetchSectionsDef } from './impl';

export const fetchSections = partial(fetchSectionsDef, [get]);
