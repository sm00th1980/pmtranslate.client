/* eslint import/prefer-default-export: off */

import {
  fetchSectionsSuccess,
} from '../../actions';

export const fetchSectionsDef = (Get) => (dispatch) => Get('/sections')
  .then((response) => {
    if (response.success) {
      dispatch(fetchSectionsSuccess(response.sections));
    } else {
      throw Error('Failed to fetch sections');
    }
  });
