import { fetchFilesDef as fetchFiles } from '../impl';
import {
  FETCH_FILES_SUCCESS,
} from '../../../constants/actions';

describe('fetchFiles()', () => {
  it('fetch files when request succeed', async () => {
    expect.assertions(4);

    const response = {
      success: true,
      files: 'files',
    };

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn().mockResolvedValueOnce(response);
    const mockDispatch = jest.fn();
    const sectionId = 1;

    await fetchFiles(mockGet, sectionId)(mockDispatch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalled();
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockDispatch.mock.calls).toEqual([
      [{ type: FETCH_FILES_SUCCESS, payload: response.files }],
    ]);

    expect(mockGet).toHaveBeenCalledWith(`/section/${sectionId}/files`);
  });

  it('not fetch files when request succeed but response not success', async () => {
    expect.assertions(4);

    const response = {
      success: false,
    };

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn().mockResolvedValueOnce(response);
    const mockDispatch = jest.fn();
    const sectionId = 1;

    await fetchFiles(mockGet, sectionId)(mockDispatch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalled();

    expect(mockDispatch).not.toHaveBeenCalled();

    expect(mockGet).toHaveBeenCalledWith(`/section/${sectionId}/files`);
  });

  it('not fetch files when request succeed but response not success', async () => {
    expect.assertions(4);

    const error = new Error('');

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn().mockRejectedValueOnce(error);
    const mockDispatch = jest.fn();
    const sectionId = 1;

    await fetchFiles(mockGet, sectionId)(mockDispatch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error);

    expect(mockDispatch).not.toHaveBeenCalled();

    expect(mockGet).toHaveBeenCalledWith(`/section/${sectionId}/files`);
  });
});
