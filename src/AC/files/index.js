/* eslint import/prefer-default-export: off */

import { partial } from 'ramda';
import { get } from '../../utils/services/request';

import { fetchFilesDef } from './impl';

export const fetchFiles = partial(fetchFilesDef, [get]);
