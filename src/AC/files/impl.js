/* eslint import/prefer-default-export: off */

import {
  fetchFilesSuccess,
} from '../../actions';

export const fetchFilesDef = (Get, sectionId) => (dispatch) => Get(`/section/${sectionId}/files`)
  .then((response) => {
    if (response.success) {
      dispatch(
        fetchFilesSuccess(response.files),
      );
    } else {
      throw Error('Failed to fetch files');
    }
  });
