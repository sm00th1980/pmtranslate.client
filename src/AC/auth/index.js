/* eslint import/prefer-default-export: off */

import { partial } from 'ramda';
import { get } from '../../utils/services/request';
import { saveAuthCredentials } from '../../utils/services/preference';

import { fetchAuthDef } from './auth';

export const fetchAuth = partial(fetchAuthDef, [get, saveAuthCredentials]);
