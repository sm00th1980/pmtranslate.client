import { fetchAuthDef as fetchAuth } from '../auth';

describe('fetchAuth()', () => {
  it('fetch auth when request succeed', async () => {
    expect.assertions(4);

    const response = {
      success: true,
      access_token: 'access_token',
    };

    const username = 'username';
    const password = 'password';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn().mockResolvedValueOnce(response);
    const mockSaveAuthCredentials = jest.fn().mockResolvedValueOnce();
    const mockDispatch = jest.fn();

    await fetchAuth(mockGet, mockSaveAuthCredentials, username, password)(mockDispatch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith(true);
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockGet).toHaveBeenCalledWith('/auth', { username, password });
    expect(mockSaveAuthCredentials).toHaveBeenCalledWith(username, response.access_token);
  });

  it('fetch auth when auth failed', async () => {
    expect.assertions(4);

    const response = {
      success: false,
    };

    const username = 'username';
    const password = 'password';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn().mockResolvedValueOnce(response);
    const mockSaveAuthCredentials = jest.fn().mockResolvedValueOnce();
    const mockDispatch = jest.fn();

    await fetchAuth(mockGet, mockSaveAuthCredentials, username, password)(mockDispatch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith(false);
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockGet).toHaveBeenCalledWith('/auth', { username, password });
    expect(mockSaveAuthCredentials).not.toHaveBeenCalled();
  });

  it('fetch auth when request failed', async () => {
    expect.assertions(4);

    const username = 'username';
    const password = 'password';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn().mockRejectedValueOnce();
    const mockSaveAuthCredentials = jest.fn().mockResolvedValueOnce();
    const mockDispatch = jest.fn();

    await fetchAuth(mockGet, mockSaveAuthCredentials, username, password)(mockDispatch)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalled();

    expect(mockGet).toHaveBeenCalledWith('/auth', { username, password });
    expect(mockSaveAuthCredentials).not.toHaveBeenCalled();
  });
});
