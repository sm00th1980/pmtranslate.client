/* eslint import/prefer-default-export: off */

export const fetchAuthDef = (Get, saveAuthCredentials, username, password) => () => {
  const params = { username, password };
  return Get('/auth', params)
    .then((response) => {
      if (response.success) {
        const { access_token: accessToken } = response;
        return saveAuthCredentials(username, accessToken).then(() => true);
      }
      return false;
    });
};
