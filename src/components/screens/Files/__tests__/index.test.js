import React from 'react';
import { shallow } from 'enzyme';
import Files from '../pure';
import { noop } from '../../../../utils';

describe('Files', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
        goBack: noop,
        fetchFiles: () => ({
          catch: noop,
        }),
        toggleTranslationApproved: noop,
        section: {
          id: 1,
          name: 'name',
          files_count: 1,
          translations_count: 1,
        },
      };

      shallow(<Files {...props} />);
    }).not.toThrow();
  });

  it('handle back', () => {
    const props = {
      goBack: jest.fn(),
      fetchFiles: () => ({
        catch: noop,
      }),
      toggleTranslationApproved: noop,
      section: {
        id: 1,
        name: 'name',
        files_count: 1,
        translations_count: 1,
      },
    };

    const component = shallow(<Files {...props} />);
    const navigationBar = component.find('NavigationBar');

    const { goBack } = props;
    expect(goBack).not.toHaveBeenCalled();
    navigationBar.simulate('back');
    expect(goBack).toHaveBeenCalled();
  });
});
