import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchFiles as fetchFilesAC } from '../../../AC/files';
import {
  toggleTranslationApproved as toggleTranslationApprovedAC,
} from '../../../AC/translation';
import Files from './pure';
import { findById } from '../../../utils';
import {
  getGoBack,
  getGetParam,
  getFiles,
  getTranslations,
  getSections,
} from '../../../selectors';

const mapStateToProps = (state, ownProps) => {
  const getParam = getGetParam(ownProps);
  const sectionId = getParam('sectionId');

  const files = getFiles(state)
    .filter((file) => file.section_id === sectionId);
  const translations = getTranslations(state);
  const sections = getSections(state);
  const section = findById(sectionId, sections);

  return {
    files,
    translations,
    section,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const goBack = getGoBack(ownProps);

  return {
    goBack,
    fetchFiles: bindActionCreators(fetchFilesAC, dispatch),
    toggleTranslationApproved: bindActionCreators(
      toggleTranslationApprovedAC,
      dispatch,
    ),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Files);
