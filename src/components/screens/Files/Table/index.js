/* eslint no-return-assign: off */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, FlatList,
} from 'react-native';
import { reject } from 'ramda';

import Alert from '../../../common/Alert';
import { BACKGROUND_COLOR } from '../../../../constants/color';
import Cell from './Cell';
import Carousel from './Carousel';
import { noop, addKeys, sampleByIndex } from '../../../../utils';
import CARD_COLORS from '../../../../constants/cardColor';

const styles = StyleSheet.create({
  container: {
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
  },
});

class Table extends Component {
  state = {
    fileIndexToShow: 0,
    togglingTranslationIds: [],
  }

  renderItem = ({ item: translation }) => {
    const { togglingTranslationIds, fileIndexToShow } = this.state;
    const isToggling = togglingTranslationIds.includes(translation.id);
    const { files } = this.props;

    const approvedColor = sampleByIndex(fileIndexToShow, files.length, CARD_COLORS);

    return (
      <Cell
        {...translation}
        isToggling={isToggling}
        onPress={() => this.handleCellPress(translation)}
        approvedColor={approvedColor}
      />
    );
  }

  renderHeader = () => {
    const { files } = this.props;
    return (
      <Carousel
        files={files}
        onSnapCard={this.handleSnapCard}
      />
    );
  }

  handleNetworkFailed = (error) => {
    this.dropdown.alertWithType('error', 'Oops', error);
  }

  handleSnapCard = (fileIndexToShow) => {
    this.setState({ fileIndexToShow });
  }

  handleCellPress = (translation) => {
    const { toggleTranslationApproved } = this.props;
    const { togglingTranslationIds } = this.state;

    this.setState({
      togglingTranslationIds: [
        ...togglingTranslationIds,
        translation.id,
      ],
    }, () => {
      toggleTranslationApproved(translation.id)
        .catch((error) => {
          this.handleNetworkFailed(error);
        })
        .finally(() => {
          // remove toggling state from cell
          const { togglingTranslationIds: translationIds } = this.state;

          this.setState({
            togglingTranslationIds: reject(
              (translationId) => translationId === translation.id,
              translationIds,
            ),
          });
        });
    });
  }

  render() {
    const {
      refreshing, onRefresh, files, translations,
    } = this.props;
    const { fileIndexToShow } = this.state;

    const file = files[fileIndexToShow];

    return (
      <>
        <FlatList
          refreshing={refreshing}
          onRefresh={onRefresh}
          style={styles.container}
          renderItem={this.renderItem}
          data={
            addKeys(
              translations.filter((translation) => translation.file_id === file.id),
            )
          }
          ListHeaderComponent={this.renderHeader}
        />
        <Alert onRef={(ref) => this.dropdown = ref} />
      </>
    );
  }
}

Table.propTypes = {
  files: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  ),
  translations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      content: PropTypes.string.isRequired,
      approved: PropTypes.bool.isRequired,
      created_at: PropTypes.number.isRequired,
      repeat_count: PropTypes.number.isRequired,
    }).isRequired,
  ),
  refreshing: PropTypes.bool,
  onRefresh: PropTypes.func,
  toggleTranslationApproved: PropTypes.func.isRequired,
};

Table.defaultProps = {
  files: [],
  translations: [],
  refreshing: false,
  onRefresh: noop,
};

export default Table;
