/* eslint camelcase: off */

import React from 'react';
import PropTypes from 'prop-types';
import { noop } from '../../../../../utils';
import { formatCreatedAt } from './utils';

import Normal from './Normal';
import Toggling from './Toggling';

const Cell = ({
  content,
  created_at: createdAt,
  repeat_count: repeatCount,
  approved,
  onPress,
  isToggling,
  approvedColor,
}) => {
  if (isToggling) {
    return (
      <Toggling
        content={content}
        createdAt={formatCreatedAt(createdAt)}
        repeatCount={repeatCount}
        approved={approved}
        approvedColor={approvedColor}
      />
    );
  }

  return (
    <Normal
      content={content}
      createdAt={formatCreatedAt(createdAt)}
      repeatCount={repeatCount}
      approved={approved}
      onPress={onPress}
      approvedColor={approvedColor}
    />
  );
};

Cell.propTypes = {
  content: PropTypes.string.isRequired,
  created_at: PropTypes.number.isRequired,
  repeat_count: PropTypes.number.isRequired,
  approved: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
  isToggling: PropTypes.bool,
  approvedColor: PropTypes.string.isRequired,
};

Cell.defaultProps = {
  onPress: noop,
  isToggling: false,
};

export default Cell;
