/* eslint camelcase: off */

import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, View, ActivityIndicator,
} from 'react-native';
import { detailText } from '../utils';

const FONT_SIZE_CONTENT = 14;
const FONT_SIZE_DETAIL = 12;

export const styles = ({ approvedColor }) => StyleSheet.create({
  container: {
    minHeight: 44,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: 'grey',
    borderRadius: 5,
    paddingRight: 10,
  },
  approvedStatus: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    width: 5,
    backgroundColor: approvedColor,
    marginRight: 5,
    opacity: 0.5,
  },
  notApprovedStatus: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    width: 5,
    backgroundColor: 'white',
    marginRight: 5,
    opacity: 0.5,
  },
  textContainer: {
    flex: 1,
    paddingTop: 5,
    paddingBottom: 5,
    opacity: 0.5,
  },
  content: {
    fontSize: FONT_SIZE_CONTENT,
    marginBottom: 5,
  },
  createdAt: {
    color: 'grey',
    fontSize: FONT_SIZE_DETAIL,
  },
  loader: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
});


const Toggling = ({
  content,
  createdAt,
  repeatCount,
  approved,
  approvedColor,
}) => {
  const style = { approvedColor };

  return (
    <View style={styles(style).container}>
      <View style={styles(style).loader}>
        <ActivityIndicator size="small" color="#0000ff" />
      </View>
      <View style={approved ? styles(style).approvedStatus : styles(style).notApprovedStatus} />
      <View style={styles(style).textContainer}>
        <Text style={styles(style).content}>
          {content}
        </Text>
        <Text style={styles(style).createdAt}>
          {detailText(createdAt, repeatCount)}
        </Text>
      </View>
    </View>
  );
};

Toggling.propTypes = {
  content: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  repeatCount: PropTypes.number.isRequired,
  approved: PropTypes.bool.isRequired,
  approvedColor: PropTypes.string.isRequired,
};

export default Toggling;
