/* eslint import/prefer-default-export: off */

import moment from 'moment';

export const formatCreatedAt = (date) => {
  const ms = parseInt(date, 10) * 1000;
  return moment(ms).fromNow();
};

export const detailText = (createdAt, repeatCount) => `Created: ${createdAt}, Repeats: ${repeatCount}`;
