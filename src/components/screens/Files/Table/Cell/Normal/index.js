/* eslint camelcase: off */

import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, TouchableOpacity, Text, View,
} from 'react-native';
import { noop } from '../../../../../../utils';
import { detailText } from '../utils';

const FONT_SIZE_CONTENT = 14;
const FONT_SIZE_DETAIL = 12;

export const styles = ({ opacity, approvedColor }) => StyleSheet.create({
  container: {
    minHeight: 44,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    flexDirection: 'row',

    borderWidth: 0.5,
    borderColor: 'grey',
    borderRadius: 5,
    opacity,
    paddingRight: 10,
  },
  approvedStatus: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    width: 5,
    backgroundColor: approvedColor,
    marginRight: 5,
  },
  notApprovedStatus: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    width: 5,
    backgroundColor: 'white',
    marginRight: 5,
  },
  textContainer: {
    flex: 1,
    paddingTop: 5,
    paddingBottom: 5,
  },
  content: {
    fontSize: FONT_SIZE_CONTENT,
    marginBottom: 5,
  },
  createdAt: {
    color: 'grey',
    fontSize: FONT_SIZE_DETAIL,
  },
});


const Normal = ({
  content,
  createdAt,
  repeatCount,
  approved,
  onPress,
  approvedColor,
}) => {
  const opacity = approved ? 1.0 : 0.41;
  const style = { opacity, approvedColor };

  return (
    <TouchableOpacity
      style={styles(style).container}
      activeOpacity={0.5}
      onPress={onPress}
    >
      <View style={approved ? styles(style).approvedStatus : styles(style).notApprovedStatus} />
      <View style={styles(style).textContainer}>
        <Text style={styles(style).content}>
          {content}
        </Text>
        <Text style={styles(style).createdAt}>
          {detailText(createdAt, repeatCount)}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

Normal.propTypes = {
  content: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  repeatCount: PropTypes.number.isRequired,
  approved: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
  approvedColor: PropTypes.string.isRequired,
};

Normal.defaultProps = {
  onPress: noop,
};

export default Normal;
