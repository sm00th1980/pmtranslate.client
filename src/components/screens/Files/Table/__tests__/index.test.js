import React from 'react';
import { shallow } from 'enzyme';
import Table from '..';
import { noop } from '../../../../../utils';

describe('Files', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
        toggleTranslationApproved: noop,
      };

      shallow(<Table {...props} />);
    }).not.toThrow();
  });
});
