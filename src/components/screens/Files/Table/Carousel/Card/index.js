import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Text,
} from 'react-native';

const FONT_SIZE = 55;

export const styles = (backgroundColor) => StyleSheet.create({
  container: {
    marginTop: 10,
    marginBottom: 10,
    flex: 1,
    backgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  title: {
    fontSize: FONT_SIZE,
    color: 'white',
  },
});

const Card = ({ title, backgroundColor }) => (
  <View style={styles(backgroundColor).container}>
    <Text style={styles(backgroundColor).title}>
      {title}
    </Text>
  </View>
);

Card.propTypes = {
  title: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
};


export default Card;
