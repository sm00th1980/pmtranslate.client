import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View,
} from 'react-native';
import RNSnapCarousel from 'react-native-snap-carousel';
import Card from './Card';
import { noop, addKeys, sampleByIndex } from '../../../../../utils';
import CARD_COLORS from '../../../../../constants/cardColor';

import { sliderWidth, itemWidth, stripFilename } from './utils';

export const styles = StyleSheet.create({
  container: {
    height: 200,
  },
});

class Carousel extends Component {
  renderItem = ({ item, index }) => {
    const { files } = this.props;
    const backgroundColor = sampleByIndex(index, files.length, CARD_COLORS);

    return (
      <Card
        title={stripFilename(item.name)}
        backgroundColor={backgroundColor}
      />
    );
  }

  handleSnapItem = (index) => {
    const { onSnapCard } = this.props;
    onSnapCard(index);
  }

  render() {
    const { files } = this.props;

    return (
      <View style={styles.container}>
        <RNSnapCarousel
          data={addKeys(files)}
          renderItem={this.renderItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          onSnapToItem={this.handleSnapItem}
        />
      </View>
    );
  }
}

Carousel.propTypes = {
  onSnapCard: PropTypes.func,
  files: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  ),
};

Carousel.defaultProps = {
  onSnapCard: noop,
  files: [],
};

export default Carousel;
