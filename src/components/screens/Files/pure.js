/* eslint no-return-assign: off */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View,
} from 'react-native';

import { isEmpty } from 'ramda';

import NavigationBar from '../../common/NavigationBar';
import Loader from '../../common/Loader';
import Alert from '../../common/Alert';
import Table from './Table';

const isFirstLoading = (items) => isEmpty(items);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

class Files extends Component {
  state = {
    refreshing: false,
    hideLoader: false,
  }

  componentDidMount() {
    const { section } = this.props;
    this.loadData(section.id);
  }

  loadData = (sectionId) => {
    const { fetchFiles } = this.props;
    fetchFiles(sectionId)
      .catch((error) => {
        this.handleNetworkFailed(error);
      });
  }

  handleBackPress = () => {
    const { goBack } = this.props;
    goBack();
  }

  handleNetworkFailed = (error) => {
    this.dropdown.alertWithType('error', 'Oops', error);
    this.setState({
      hideLoader: true,
    });
  }

  handleRefresh = () => {
    const { fetchFiles, section: { id } } = this.props;

    this.setState({ refreshing: true });
    fetchFiles(id)
      .catch((error) => {
        this.handleNetworkFailed(error);
      })
      .finally(() => {
        this.setState({ refreshing: false });
      });
  }

  render() {
    const { refreshing, hideLoader } = this.state;
    const {
      files, translations, section, toggleTranslationApproved,
    } = this.props;

    return (
      <View style={styles.container}>
        {
          !hideLoader
          && <Loader loading={isFirstLoading(files)} />
        }
        <NavigationBar
          onBack={this.handleBackPress}
          title1={section.name}
          title2={`Files: ${section.files_count}, Translations: ${section.translations_count}`}
        />
        {
          !isEmpty(files)
          && (
          <Table
            files={files}
            translations={translations}
            refreshing={refreshing}
            onRefresh={this.handleRefresh}
            toggleTranslationApproved={toggleTranslationApproved}
          />
          )
        }
        <Alert onRef={(ref) => this.dropdown = ref} />
      </View>
    );
  }
}

Files.propTypes = {
  fetchFiles: PropTypes.func.isRequired,
  files: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  ),
  translations: PropTypes.arrayOf(
    PropTypes.shape(),
  ),
  section: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    files_count: PropTypes.number.isRequired,
    translations_count: PropTypes.number.isRequired,
  }).isRequired,
  goBack: PropTypes.func.isRequired,
  toggleTranslationApproved: PropTypes.func.isRequired,
};

Files.defaultProps = {
  files: [],
  translations: [],
};

export default Files;
