import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View, StyleSheet, Text,
} from 'react-native';

import { noop } from '../../../../utils';
import TextInput from '../../../common/TextInput';

const HEIGHT = 60;
const PADDING = 15;

const styles = (style) => StyleSheet.create({
  container: {
    height: HEIGHT,
    backgroundColor: 'white',
    paddingLeft: PADDING,
    paddingRight: PADDING,
    marginBottom: 15,
    ...style,
  },
  text: {
    fontSize: 16,
    color: 'grey',
  },
});

class PasswordInput extends Component {
  handleChangePassword = (newPassword) => {
    const { onChangePassword } = this.props;
    onChangePassword(newPassword);
  }

  render() {
    const { style, password } = this.props;
    return (
      <View style={styles(style).container}>
        <Text style={styles(style).text}>
        Password
        </Text>
        <TextInput
          value={password}
          placeholder="Your password"
          onChangeText={this.handleChangePassword}
          secureTextEntry
        />
      </View>
    );
  }
}

PasswordInput.propTypes = {
  password: PropTypes.string,
  style: PropTypes.shape(),
  onChangePassword: PropTypes.func,
};

PasswordInput.defaultProps = {
  password: '',
  style: {},
  onChangePassword: noop,
};

export default PasswordInput;
