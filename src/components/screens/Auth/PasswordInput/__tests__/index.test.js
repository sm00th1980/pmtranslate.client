import React from 'react';
import { shallow } from 'enzyme';
import PasswordInput from '..';

describe('PasswordInput', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
      };
      const component = shallow(<PasswordInput {...props} />);
      expect(component).toBeTruthy();
    }).not.toThrow();
  });
});
