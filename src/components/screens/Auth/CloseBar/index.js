import React from 'react';
import PropTypes from 'prop-types';
import {
  View, StyleSheet, Image, TouchableOpacity, Text,
} from 'react-native';
import FONT_WEIGHT from '../../../../constants/fontWeight';

const source = require('./close.png');

const IMAGE_SIZE = 20;
const HEIGHT = 50;
const PADDING = 15;
const WIDTH = IMAGE_SIZE + 2 * PADDING;

const styles = (style) => StyleSheet.create({
  container: {
    height: HEIGHT,
    flexDirection: 'row',
    paddingRight: WIDTH,
    marginBottom: 15,
    ...style,
  },
  back: {
    width: IMAGE_SIZE,
    height: IMAGE_SIZE,
  },
  touch: {
    paddingLeft: PADDING,
    justifyContent: 'center',
    width: WIDTH,
  },
  textContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 18,
    fontWeight: FONT_WEIGHT.MEDIUM,
  },
});

const CloseBar = ({ onClose, style }) => (
  <View style={styles(style).container}>
    <TouchableOpacity
      style={styles(style).touch}
      activeOpacity={0.5}
      onPress={onClose}
    >
      <Image style={styles(style).back} source={source} />
    </TouchableOpacity>
    <View style={styles(style).textContainer}>
      <Text style={styles(style).text}>
        Sign in
      </Text>
    </View>
  </View>
);

CloseBar.propTypes = {
  onClose: PropTypes.func.isRequired,
  style: PropTypes.shape(),
};

CloseBar.defaultProps = {
  style: {},
};

export default CloseBar;
