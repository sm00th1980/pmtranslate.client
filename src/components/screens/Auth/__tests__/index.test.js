import React from 'react';
import { shallow } from 'enzyme';
import Auth from '../pure';
import { noop } from '../../../../utils';

describe('Auth', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
        goBack: noop,
        fetchAuth: noop,
      };

      shallow(<Auth {...props} />);
    }).not.toThrow();
  });

  it('handle back', () => {
    const props = {
      goBack: jest.fn(),
      fetchAuth: noop,
    };

    const component = shallow(<Auth {...props} />);
    const closeBtn = component.find('CloseBar');

    const { goBack } = props;
    expect(goBack).not.toHaveBeenCalled();
    closeBtn.simulate('close');
    expect(goBack).toHaveBeenCalled();
  });
});
