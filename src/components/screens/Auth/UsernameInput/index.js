import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View, StyleSheet, Text,
} from 'react-native';

import { noop } from '../../../../utils';
import TextInput from '../../../common/TextInput';

const PADDING = 15;

const styles = (style) => StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingLeft: PADDING,
    paddingRight: PADDING,
    ...style,
  },
  text: {
    fontSize: 16,
    color: 'grey',
  },
});

class UsernameInput extends Component {
  handleChangeUsername = (newUsername) => {
    const { onChangeUsername } = this.props;
    onChangeUsername(newUsername);
  }

  render() {
    const { style, username } = this.props;
    return (
      <View style={styles(style).container}>
        <Text style={styles(style).text}>
          Username
        </Text>
        <TextInput
          value={username}
          placeholder="Your username"
          onChangeText={this.handleChangeUsername}
        />
      </View>
    );
  }
}

UsernameInput.propTypes = {
  username: PropTypes.string,
  style: PropTypes.shape(),
  onChangeUsername: PropTypes.func,
};

UsernameInput.defaultProps = {
  username: '',
  style: {},
  onChangeUsername: noop,
};

export default UsernameInput;
