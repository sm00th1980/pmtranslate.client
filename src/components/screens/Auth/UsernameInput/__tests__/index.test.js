import React from 'react';
import { shallow } from 'enzyme';
import UsernameInput from '..';

describe('UsernameInput', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
      };
      const component = shallow(<UsernameInput {...props} />);
      expect(component).toBeTruthy();
    }).not.toThrow();
  });
});
