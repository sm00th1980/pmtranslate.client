import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Text } from 'react-native';

import UsernameInput from './UsernameInput';
import PasswordInput from './PasswordInput';
import Button from '../../common/Button';
import CloseBar from './CloseBar';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  button: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 20,

    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 3,
    shadowOpacity: 0.6,
  },
  username: {
    marginBottom: 35,
  },
  textError: {
    padding: 10,
    fontSize: 15,
    textAlign: 'center',
    color: 'red',
  },
});

class AuthScreen extends Component {
  state = {
    showAuthError: false,
    showNetworkError: false,
    isAuthorizing: false,
    username: '',
    password: '',
  }

  handleBack = () => {
    const { goBack } = this.props;
    goBack();
  }

  handleChangeUsername = (username) => {
    this.setState({ username });
  }

  handleChangePassword = (password) => {
    this.setState({ password });
  }

  handleAuth = () => {
    const { fetchAuth } = this.props;
    const { username, password } = this.state;

    this.setState({ isAuthorizing: true });
    fetchAuth(username, password)
      .then((isAuth) => {
        if (isAuth) {
          this.handleBack();
        } else {
          this.setState({ showAuthError: true });
        }
      })
      .catch(() => {
        this.setState({ showNetworkError: true });
      })
      .finally(() => {
        this.setState({ isAuthorizing: false });
      });
  }

  render() {
    const {
      username, password, showAuthError, showNetworkError, isAuthorizing,
    } = this.state;

    return (
      <View style={styles.container}>
        <CloseBar onClose={this.handleBack} />
        <UsernameInput
          style={styles.username}
          username={username}
          onChangeUsername={this.handleChangeUsername}
        />
        <PasswordInput
          password={password}
          onChangePassword={this.handleChangePassword}
        />
        <Button
          style={styles.button}
          onPress={this.handleAuth}
          isLoading={isAuthorizing}
          title="LOGIN"
        />
        {
          showAuthError
          && (
          <Text style={styles.textError}>
            Wrong username or password
          </Text>
          )
        }
        {
          showNetworkError
          && (
          <Text style={styles.textError}>
            Failed to reach server
          </Text>
          )
        }
      </View>
    );
  }
}

AuthScreen.propTypes = {
  goBack: PropTypes.func.isRequired,
  fetchAuth: PropTypes.func.isRequired,
};

export default AuthScreen;
