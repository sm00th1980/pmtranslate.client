import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchAuth as fetchAuthAC } from '../../../AC/auth';
import AuthScreen from './pure';
import { getGoBack } from '../../../selectors';

const mapDispatchToProps = (dispatch, ownProps) => {
  const goBack = getGoBack(ownProps);

  return {
    goBack,
    fetchAuth: bindActionCreators(fetchAuthAC, dispatch),
  };
};

export default connect(undefined, mapDispatchToProps)(AuthScreen);
