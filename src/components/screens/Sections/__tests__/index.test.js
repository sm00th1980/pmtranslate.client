import React from 'react';
import { shallow } from 'enzyme';
import Sections from '../pure';
import { noop } from '../../../../utils';

describe('Sections', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
        navigateToFiles: noop,
        fetchSections: () => ({
          catch: jest.fn(),
        }),
      };

      shallow(<Sections {...props} />);
    }).not.toThrow();
  });
});
