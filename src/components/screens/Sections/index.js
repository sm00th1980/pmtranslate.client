import { partial } from 'ramda';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchSections as fetchSectionsAC } from '../../../AC/sections';
import Sections from './pure';
import {
  getSections,
  getNavigate,
} from '../../../selectors';

const mapStateToProps = (state) => ({
  sections: getSections(state),
});

const mapDispatchToProps = (dispatch, ownProps) => {
  const navigate = getNavigate(ownProps);

  return {
    navigateToFiles: partial(navigate, ['Files']),
    fetchSections: bindActionCreators(fetchSectionsAC, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sections);
