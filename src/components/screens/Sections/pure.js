/* eslint no-return-assign: off */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, FlatList,
} from 'react-native';

import { isNil, splitEvery } from 'ramda';

import { BACKGROUND_COLOR } from '../../../constants/color';
import Loader from '../../common/Loader';
import Alert from '../../common/Alert';

import { noop } from '../../../utils';

import Cell from './Cell';
import Header from './Header';
import Footer from './Footer';
import ItemSeparator from './ItemSeparator';
import Search from './Search';
import { filterByName } from './utils';

const SPLIT_COUNT = 2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  },
  table: {
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
  },
});

class Sections extends Component {
  state = {
    refreshing: false,
    searchValue: undefined,
    hideLoader: false,
  }

  componentDidMount() {
    const { fetchSections } = this.props;
    fetchSections()
      .catch((error) => {
        this.handleNetworkFailed(error);
      });
  }

  handleCellTap = (section) => {
    const { navigateToFiles } = this.props;

    navigateToFiles({ sectionId: section.id });
  }

  handleRefresh = () => {
    const { fetchSections } = this.props;
    this.setState({ refreshing: true });
    fetchSections()
      .catch((error) => {
        this.handleNetworkFailed(error);
      })
      .finally(() => {
        this.setState({ refreshing: false });
      });
  }

  handleSearchChangeText = (searchValue) => {
    this.setState({ searchValue });
  }

  handleNetworkFailed = (error) => {
    this.dropdown.alertWithType('error', 'Oops', error);
    this.setState({
      hideLoader: true,
    });
  }

  render() {
    const { sections } = this.props;
    const { refreshing, searchValue, hideLoader } = this.state;

    const isFirstLoading = isNil(sections);

    const data = splitEvery(SPLIT_COUNT, filterByName(sections || [], searchValue));
    const sectionsLength = (sections || []).length;

    return (
      <View style={styles.container}>
        {
          !hideLoader
          && <Loader loading={isFirstLoading} />
        }
        <Search
          onChangeText={this.handleSearchChangeText}
          value={searchValue}
        />

        {
          !isFirstLoading
          && (
            <FlatList
              style={styles.table}
              renderItem={({ item: [section1, section2], index }) => (
                <Cell
                  section1Index={index * SPLIT_COUNT}
                  section2Index={index * SPLIT_COUNT + 1}
                  sectionsLength={sectionsLength}
                  section1={section1}
                  onPress1={() => this.handleCellTap(section1)}
                  section2={section2}
                  onPress2={section2 ? () => this.handleCellTap(section2) : noop}
                />
              )}
              data={data}
              onRefresh={this.handleRefresh}
              refreshing={refreshing}
              ItemSeparatorComponent={ItemSeparator}
              ListHeaderComponent={Header}
              ListFooterComponent={Footer}
              keyExtractor={(item, index) => index.toString()}
            />
          )
        }
        <Alert onRef={(ref) => this.dropdown = ref} />
      </View>
    );
  }
}

Sections.propTypes = {
  navigateToFiles: PropTypes.func.isRequired,
  fetchSections: PropTypes.func.isRequired,
  sections: PropTypes.arrayOf(PropTypes.shape()),
};

Sections.defaultProps = {
  sections: undefined,
};

export default Sections;
