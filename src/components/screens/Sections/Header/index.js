import React from 'react';
import { View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 8,
  },
});

const Header = () => (
  <View style={styles.container} />
);

export default Header;
