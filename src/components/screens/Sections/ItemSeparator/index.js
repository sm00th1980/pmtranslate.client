import React from 'react';
import { View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 10,
    // backgroundColor: 'white',
  },
});

const ItemSeparator = () => (
  <View style={styles.container} />
);

export default ItemSeparator;
