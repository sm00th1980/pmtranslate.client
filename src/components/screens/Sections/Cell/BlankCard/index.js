import React from 'react';
import {
  StyleSheet, View,
} from 'react-native';

const PADDING = 8;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginRight: PADDING / 2.0,
    padding: 10,
  },
});

const BlankCard = () => (
  <View style={styles.container} />
);

export default BlankCard;
