import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View,
} from 'react-native';
import { isNil } from 'ramda';
import { noop, sampleByIndex } from '../../../../utils';
import CARD_COLORS from '../../../../constants/cardColor';

import LeftCard from './LeftCard';
import RightCard from './RightCard';
import BlankCard from './BlankCard';

const PADDING = 8;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: PADDING,
    paddingRight: PADDING,
    flexDirection: 'row',
  },
});

const Cell = ({
  section1,
  onPress1,
  section2,
  onPress2,
  section1Index,
  section2Index,
  sectionsLength,
}) => (
  <View style={styles.container}>
    <LeftCard
      section={section1}
      onPress={onPress1}
      color={sampleByIndex(section1Index, sectionsLength, CARD_COLORS)}
    />
    {
        isNil(section2) && <BlankCard />
      }
    {
        !isNil(section2)
        && (
        <RightCard
          section={section2}
          onPress={onPress2}
          color={sampleByIndex(section2Index, sectionsLength, CARD_COLORS)}
        />
        )
      }
  </View>
);

Cell.propTypes = {
  section1Index: PropTypes.number.isRequired,
  section2Index: PropTypes.number.isRequired,
  sectionsLength: PropTypes.number.isRequired,
  onPress1: PropTypes.func,
  onPress2: PropTypes.func,
  section1: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    files_count: PropTypes.number.isRequired,
    translations_count: PropTypes.number.isRequired,
  }).isRequired,
  section2: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    files_count: PropTypes.number.isRequired,
    translations_count: PropTypes.number.isRequired,
  }),
};

Cell.defaultProps = {
  onPress1: noop,
  onPress2: noop,
  section2: undefined,
};

export default Cell;
