import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, TouchableOpacity, Text,
} from 'react-native';
import { noop } from '../../../../../utils';

const FONT_SIZE_TITLE = 20;

const PADDING = 8;

export const styles = (backgroundColor) => StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 10,
    marginRight: PADDING / 2.0,
    padding: 10,

    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 3,
    shadowOpacity: 0.5,
    backgroundColor,
  },
  title: {
    fontSize: FONT_SIZE_TITLE,
    marginBottom: 20,
    color: 'white',
  },
  files: {
    color: 'white',
    fontSize: 14,
    marginBottom: 2,
  },
  translations: {
    color: 'white',
    fontSize: 14,
  },
});

const LeftCard = ({
  onPress,
  section,
  color,
}) => (
  <TouchableOpacity
    style={styles(color).container}
    activeOpacity={0.5}
    onPress={onPress}
  >
    <Text style={styles(color).title} ellipsizeMode="tail" numberOfLines={1}>
      {section.name}
    </Text>
    <Text style={styles(color).files}>
      Files:
      {' '}
      {section.files_count}
    </Text>
    <Text style={styles(color).translations}>
      Translations:
      {' '}
      {section.translations_count}
    </Text>
  </TouchableOpacity>
);

LeftCard.propTypes = {
  color: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  section: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    files_count: PropTypes.number.isRequired,
    translations_count: PropTypes.number.isRequired,
  }).isRequired,
};

LeftCard.defaultProps = {
  onPress: noop,
};

export default LeftCard;
