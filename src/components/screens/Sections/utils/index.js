/* eslint import/prefer-default-export: off */

import { isNil, includes } from 'ramda';

export const filterByName = (sections, name) => {
  if (isNil(name) || name === '') {
    return sections;
  }

  return sections
    .filter((section) => includes(
      name.toLowerCase(),
      section.name.toLowerCase(),
    ));
};
