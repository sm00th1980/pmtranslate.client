import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { SearchBar } from 'react-native-elements';

import { noop } from '../../../../utils';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  inputContainer: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 3,
    shadowOpacity: 0.2,
  },
  cancelButton: {
    color: '#3B4664',
  },
});

const Search = ({ onChangeText, value }) => (
  <SearchBar
    platform="ios"
    lightTheme
    placeholder="Type section name..."
    onChangeText={onChangeText}
    value={value}
    containerStyle={styles.container}
    inputContainerStyle={styles.inputContainer}
    cancelButtonProps={styles.cancelButton}
  />
);

Search.propTypes = {
  onChangeText: PropTypes.func,
  value: PropTypes.string,
};

Search.defaultProps = {
  onChangeText: noop,
  value: undefined,
};

export default Search;
