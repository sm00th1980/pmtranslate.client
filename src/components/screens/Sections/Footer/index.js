import React from 'react';
import { View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 8,
  },
});

const Footer = () => (
  <View style={styles.container} />
);

export default Footer;
