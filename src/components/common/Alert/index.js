import React from 'react';
import PropTypes from 'prop-types';
import DropdownAlert from 'react-native-dropdownalert';

const Alert = ({ onRef }) => (
  <DropdownAlert ref={onRef} />
);

Alert.propTypes = {
  onRef: PropTypes.func.isRequired,
};

export default Alert;
