import React from 'react';
import PropTypes from 'prop-types';
import RNMLoader from 'react-native-modal-loader';

const Loader = ({ loading }) => (
  <RNMLoader loading={loading} color="#ff66be" size="large" />
);

Loader.propTypes = {
  loading: PropTypes.bool,
};

Loader.defaultProps = {
  loading: false,
};

export default Loader;
