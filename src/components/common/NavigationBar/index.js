import React from 'react';
import PropTypes from 'prop-types';
import {
  View, StyleSheet, Image, TouchableOpacity, Text,
} from 'react-native';

const source = require('./back.png');

const IMAGE_SIZE = 25;
const FONT_SIZE1 = 15;
const FONT_SIZE2 = 12;

const styles = (style) => StyleSheet.create({
  containerWithBack: {
    height: 64,
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10 + IMAGE_SIZE,
    flexDirection: 'row',
    ...style,
  },
  containerWithoutBack: {
    height: 64,
    paddingTop: 20,
    paddingLeft: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: '#d6d7da',
    paddingRight: 10,
    flexDirection: 'row',
    ...style,
  },
  back: {
    width: IMAGE_SIZE,
    height: IMAGE_SIZE,
  },
  touch: {
    alignItems: 'center',
    justifyContent: 'center',
    width: IMAGE_SIZE,
  },
  titleContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title1: {
    fontSize: FONT_SIZE1,
    color: 'black',
  },
  title2: {
    fontSize: FONT_SIZE2,
    color: '#979797',
  },
});

const NavigationBar = ({
  onBack, style, title1, title2,
}) => (
  <View style={onBack ? styles(style).containerWithBack : styles(style).containerWithoutBack}>
    {
      onBack && (
      <TouchableOpacity
        style={styles(style).touch}
        activeOpacity={0.5}
        onPress={onBack}
      >
        <Image style={styles(style).back} source={source} />
      </TouchableOpacity>
      )
    }
    <View style={styles(style).titleContainer}>
      <Text style={styles(style).title1}>
        {title1}
      </Text>
      {
        title2 && (
        <Text style={styles(style).title2}>
          {title2}
        </Text>
        )
      }
    </View>
  </View>
);

NavigationBar.propTypes = {
  onBack: PropTypes.func,
  style: PropTypes.shape(),
  title1: PropTypes.string,
  title2: PropTypes.string,
};

NavigationBar.defaultProps = {
  onBack: undefined,
  style: {},
  title1: undefined,
  title2: undefined,
};

export default NavigationBar;
