import React from 'react';
import { shallow } from 'enzyme';
import NavigationBar from '..';
import { noop } from '../../../../utils';

describe('NavigationBar', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
        onBack: noop,
      };

      const component = shallow(<NavigationBar {...props} />);
      expect(component).toBeTruthy();
    }).not.toThrow();
  });

  it('handle press back', () => {
    const props = {
      onBack: jest.fn(),
    };

    const component = shallow(<NavigationBar {...props} />);
    const { onBack } = props;
    expect(onBack).not.toHaveBeenCalled();
    const touchArea = component.find('TouchableOpacity');
    touchArea.simulate('press');
    expect(onBack).toHaveBeenCalled();
  });
});
