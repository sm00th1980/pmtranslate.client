import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, TouchableOpacity, ActivityIndicator,
} from 'react-native';

import { noop } from '../../../utils';

const BORDER_RADIUS = 10;
const HEIGHT = 55;
const FONT_SIZE = 20;

const styles = (style) => StyleSheet.create({
  ready: {
    backgroundColor: '#4b53cd',
    borderRadius: BORDER_RADIUS,
    height: HEIGHT,
    justifyContent: 'center',
    ...style,
  },

  loading: {
    backgroundColor: '#c4c4c4',
    borderRadius: BORDER_RADIUS,
    height: HEIGHT,
    justifyContent: 'center',
    ...style,
  },

  text: {
    color: '#fff',
    textAlign: 'center',
    fontSize: FONT_SIZE,
  },
});

const Button = ({
  title, onPress, style, isLoading,
}) => {
  if (isLoading) {
    return (
      <TouchableOpacity
        style={styles(style).loading}
        activeOpacity={1.0}
        onPress={noop}
      >
        <ActivityIndicator animating color="black" size="small" />
      </TouchableOpacity>
    );
  }

  return (
    <TouchableOpacity
      style={styles(style).ready}
      activeOpacity={0.5}
      onPress={onPress}
    >
      <Text style={styles(style).text}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  style: PropTypes.shape(),
  isLoading: PropTypes.bool,
};

Button.defaultProps = {
  onPress: noop,
  isLoading: false,
  style: {},
};

export default Button;
