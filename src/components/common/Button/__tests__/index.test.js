import React from 'react';
import { shallow } from 'enzyme';
import Button from '..';

describe('Button', () => {
  it('render without errors', () => {
    expect(() => {
      const props = {
        title: 'title',
      };

      const component = shallow(<Button {...props} />);
      expect(component).toBeTruthy();
    }).not.toThrow();
  });

  it('handle press', () => {
    const props = {
      title: 'title',
      onPress: jest.fn(),
    };

    const component = shallow(<Button {...props} />);
    const { onPress } = props;
    expect(onPress).not.toHaveBeenCalled();
    const touchArea = component.find('TouchableOpacity');
    touchArea.simulate('press');
    expect(onPress).toHaveBeenCalled();
  });

  it('not handle press when loading', () => {
    const props = {
      title: 'title',
      onPress: jest.fn(),
      isLoading: true,
    };

    const component = shallow(<Button {...props} />);
    const { onPress } = props;
    expect(onPress).not.toHaveBeenCalled();
    const touchArea = component.find('TouchableOpacity');
    touchArea.simulate('press');
    expect(onPress).not.toHaveBeenCalled();
  });
});
