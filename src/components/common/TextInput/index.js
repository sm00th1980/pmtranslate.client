import React from 'react';
import PropTypes from 'prop-types';
import {
  TextInput as RNTextInput, StyleSheet,
} from 'react-native';
import { noop } from '../../../utils';

const FONT_SIZE = 20;
const INPUT_HEIGHT = 45;

const styles = StyleSheet.create({
  input: {
    height: INPUT_HEIGHT,
    fontSize: FONT_SIZE,
    borderBottomWidth: 0.5,
    borderBottomColor: 'grey',
    paddingLeft: 0,
    paddingRight: 0,
    color: 'black',
  },
});

const TextInput = ({
  value, placeholder, onChangeText, secureTextEntry,
}) => (
  <RNTextInput
    style={styles.input}
    value={value}
    placeholder={placeholder}
    onChangeText={onChangeText}
    secureTextEntry={secureTextEntry}
    placeholderTextColor="#c4c4c4"
  />
);

TextInput.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChangeText: PropTypes.func,
  secureTextEntry: PropTypes.bool,
};

TextInput.defaultProps = {
  value: '',
  placeholder: '',
  onChangeText: noop,
  secureTextEntry: false,
};

export default TextInput;
