import BluebirdPromise from 'bluebird';

export default () => {
  // setup bluebird
  global.Promise = BluebirdPromise;
  global.Promise.config({
    warnings: true,
    longStackTraces: true,
    cancellation: true,
    monitoring: true,
  });
};
