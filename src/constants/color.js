export const BACKGROUND_COLOR = 'white';
export const SECTION_TEXT_COLOR = '#9b9b9b';

export const COLOR = {
  UNKNOWN: '#c4c4c4',
  SUCCESS: '#85cc9b',
  FAILED: '#e6372a',
  RUNNING: '#eff171',
  SCHEDULED: '#75caee',
  WARNING: '#eff171',
};

export const COLORS = [
  COLOR.UNKNOWN,
  COLOR.SUCCESS,
  COLOR.FAILED,
  COLOR.RUNNING,
  COLOR.SCHEDULED,
  COLOR.WARNING,
];
