export default {
  THIN: '100',
  ULTRA_LIGHT: '200',
  LIGHT: '300',
  REGULAR: '400',
  MEDIUM: '500',
  SEMIBOLD: '600',
  BOLD: '700',
  HEAVY: '800',
  BLACK: '900',
};
